===========================
INSTRUCTIONS FOR EXECUTION
===========================
Code written for MySQL in PHP
by Adrian Ochoa

Running 'php slots.php' in a terminal will connect to the mysql database
'slot_schema' and then prompt the user for id, salt and bet.

Note that salt must be in single quotes '' and can be found 
in the database. id and salt must match in order for spin to
update database.

After the spin, user either receives 0x, 1x or 2x the bet and the
mysql table 'player' is updated accordingly. A JSON response is
displayed on the terminal showing the updated data.

The table 'player' is initialized with 4 new players:

+----+----------+---------+-------+----------------+------------+
| id | name     | credits | spins | average_return | salt       |
+----+----------+---------+-------+----------------+------------+
|  1 | Adrian   |      20 |     0 |              0 | e89b165b85 |
|  2 | Watson   |      20 |     0 |              0 | a5cc416d6a |
|  3 | Queenpin |      20 |     0 |              0 | a07ada372c |
|  4 | Shark    |      20 |     0 |              0 | 26741b2842 |
+----+----------+---------+-------+----------------+------------+
