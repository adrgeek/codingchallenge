===========================
INSTRUCTIONS FOR EXECUTION
===========================
Code written for Python 3 and matploblib plotting library
by Adrian Ochoa

Navigate to folder in terminal and run problem1.py through
a python 3 interpreter:
->python problem1.py

You will see the following output for the given dataset:

->The years attaining the maximum number of people, 13, are as follows:
[1926, 1927, 1933, 1934, 1935]

You will see two plots in sequence which are also included
in this folder. First, a plot counting the people in each year
and second, a plot of the year range for each person.

New random data can be generated with the problem1data.py script.
Altering the numPeople variable will change the number of people 
for which data is generated:

->python problem1data.py

->Generated 25 intervals of year data in data.txt

Other data should work as well, so long as data.txt contains
a space separated start year/end year pair on each line.