'''--------------PROBLEM 1 DATA---------------
Generates a list of tuples for problem 1 using
random data. Modify numPeople to create bigger
datasets Created by Adrian Ochoa
-------------------------------------------'''

import random

# number of people in list
numPeople = 25

# iterable of possible years
posYears = range(1900,2001)

# create list with random start years
startYears = random.choices(posYears, k = numPeople)

# create list with random end years
endYears = [random.randint(startYear,2000) for startYear in startYears]

# list of tuples containing start and end years
yearTups = list(zip(startYears, endYears))

# write yearTups to a text file
with open('data.txt', 'w') as fileObj:
    fileObj.write('\n'.join('%s %s' % x for x in yearTups))

print("Generated {} intervals of year data in data.txt".format(numPeople))
