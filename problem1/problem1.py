'''-------------------PROBLEM 1----------------------
Given a list of people with their birth and end years
(all between 1900 and 2000), find the year with the
most number of people alive. Created by Adrian Ochoa
---------------------------------------------------'''

import matplotlib.pyplot as plt

# rangeCount is a function counting the
# number of intervals in the list 
# extremeVals that contain queryVal

def rangeCount(queryVal, extremeVals):
    count = 0
    for minVal, maxVal in extremeVals:
        if minVal <= queryVal <= maxVal:
            count += 1
    return count

# alternative functional programming 
# implementation of rangeCount
#def rangeCount(queryVal, extremeVals):
#    validIntervals = filter(lambda y: y[0] <= queryVal <= y[1], extremeVals)
#    return len(list(validIntervals))

# iterable of possible years
posYears = range(1900,2001)

# read data and pack into list of
# (startYear, endYear) tuples
yearTups = []

with open('data.txt', 'r') as fileObj:
    for row in fileObj.readlines():
        temp = row.split(" ")
        yearTups.append((int(temp[0]),int(temp[1])))

# count number of people in each year using rangeCount
yearCounts = [rangeCount(year, yearTups) for year in posYears]

# find maximum number of people in a year
maxCount = max(yearCounts)

# fetch indices of those years attaining max
indxYears = [i for i, x in enumerate(yearCounts) if x == maxCount]

# find those years attaining max
maxYears = [posYears[i] for i in indxYears]

print("The years attaining the maximum number of people, {}, are as follows:".format(maxCount))
print(maxYears)

'''-----------
Plots of data
-----------'''

plt.rcdefaults()
plt.title('Problem 1 Plot: Number of People by Year')

plt.bar(posYears, yearCounts, align='center', alpha=0.5)
plt.xlabel('Year')
plt.ylabel('Number of people')

plt.show()

plt.rcdefaults()
plt.title('Problem 1 Plot: Year Ranges by Person')

plt.boxplot(yearTups)
plt.xlabel('Person number')
plt.ylabel('Year range')
 
plt.show()